<?php
declare(strict_types=1);


namespace App\Tests\ValueObjects;

use Domain\ValueObject\Password;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class PasswordTest extends TestCase
{
    public function validInputDataProvider(): array
    {
        return [
            ['$2y$10$UBvvLeUKMzmOoomYAyn8YOKToxkNbEWz5.ac0CKnuzxge1I6AeBAu'],
            ['$2y$10$iAcSSgbqL5Dqim594xLFJ.iKMej1vaTdfXvl7cM3y0AuXCYcxMLKO']
        ];
    }

    public function validInputDataProviderFromUnhashedString(): array
    {
        return [
            ["Asdf123!"],
            ["!Test!5"]
        ];
    }

    /**
     * @test
     */
    public function factoryConstructionException(): void
    {
        $this->expectException(InvalidArgumentException::class);
        Password::fromHashedString("Asdf!234");
    }

    /**
     * @test
     */
    public function factoryConstructionExceptionFromUnhashedString(): void
    {
        $this->expectException(InvalidArgumentException::class);
        Password::fromUnhashedString("123456");
    }

    /**
     * @dataProvider validInputDataProvider
     * @test
     */
    public function factoryConstruction(string $input) : void
    {
        $password = Password::fromHashedString($input);
        $this->assertInstanceOf(Password::class, $password);
    }

    /**
     * @dataProvider validInputDataProviderFromUnhashedString
     * @test
     */
    public function factoryConstructionFromUnhashedString(string $input) : void
    {
        $password = Password::fromUnhashedString($input);
        $this->assertInstanceOf(Password::class, $password);
    }
}
