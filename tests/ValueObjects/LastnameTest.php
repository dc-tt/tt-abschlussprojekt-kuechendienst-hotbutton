<?php
declare(strict_types=1);


namespace App\Tests\ValueObjects;

use Domain\ValueObject\Lastname;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class LastnameTest extends TestCase
{
    public function validInputDataProvider(): array
    {
        return [
            ["example"],
            ["test"]
        ];
    }

    /**
     * @test
     */
    public function factoryConstructionException(): void
    {
        $this->expectException(InvalidArgumentException::class);
        Lastname::fromString("123");
    }

    /**
     * @dataProvider validInputDataProvider
     * @test
     */
    public function factoryConstruction(string $input) : void
    {
        $lastname = Lastname::fromString($input);
        $this->assertInstanceOf(Lastname::class, $lastname);
    }
}
