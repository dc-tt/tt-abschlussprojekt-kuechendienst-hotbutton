<?php
declare(strict_types=1);


namespace App\Tests\ValueObjects;

use Domain\ValueObject\Name;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class NameTest extends TestCase
{
    public function validInputDataProvider(): array
    {
        return [
            ["example"],
            ["test"],
            ["team_1234"],
        ];
    }

    /**
     * @test
     */
    public function factoryConstructionException(): void
    {
        $this->expectException(InvalidArgumentException::class);
        Name::fromString("");
    }

    /**
     * @dataProvider validInputDataProvider
     * @test
     */
    public function factoryConstruction(string $input) : void
    {
        $name = Name::fromString($input);
        $this->assertInstanceOf(Name::class, $name);
    }
}
