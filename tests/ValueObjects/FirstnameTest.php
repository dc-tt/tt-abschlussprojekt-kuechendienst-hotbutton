<?php
declare(strict_types=1);


namespace App\Tests\ValueObjects;

use Domain\ValueObject\Firstname;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class FirstnameTest extends TestCase
{
    public function validInputDataProvider(): array
    {
        return [
            ["example"],
            ["test"]
        ];
    }

    /**
     * @test
     */
    public function factoryConstructionException(): void
    {
        $this->expectException(InvalidArgumentException::class);
        Firstname::fromString("123");
    }

    /**
     * @dataProvider validInputDataProvider
     * @test
     */
    public function factoryConstruction(string $input) : void
    {
        $firstname = Firstname::fromString($input);
        $this->assertInstanceOf(Firstname::class, $firstname);
    }
}
