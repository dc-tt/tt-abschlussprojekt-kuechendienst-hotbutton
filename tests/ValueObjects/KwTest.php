<?php
declare(strict_types=1);


namespace App\Tests\ValueObjects;

use Domain\ValueObject\Kw;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class KwTest extends TestCase
{
    public function validInputDataProviderFromInteger(): array
    {
        return [[1], [2]];
    }

    public function validInputDataProviderFromString(): array
    {
        return [["1"], ["2"]];
    }

    /**
     * @test
     */
    public function factoryConstructionExceptionFromInteger(): void
    {
        $this->expectException(InvalidArgumentException::class);
        Kw::fromInteger(-1);
    }

    /**
     * @test
     */
    public function factoryConstructionExceptionFromString(): void
    {
        $this->expectException(InvalidArgumentException::class);
        Kw::fromString("-1");
    }

    /**
     * @dataProvider validInputDataProviderFromInteger
     * @test
     */
    public function factoryConstructionFromInteger(int $input) : void
    {
        $kw = Kw::fromInteger($input);
        $this->assertInstanceOf(Kw::class, $kw);
    }

    /**
     * @dataProvider validInputDataProviderFromString
     * @test
     */
    public function factoryConstructionFromString(string $input) : void
    {
        $kw = Kw::fromString($input);
        $this->assertInstanceOf(Kw::class, $kw);
    }
}
