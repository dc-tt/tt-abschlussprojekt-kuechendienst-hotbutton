<?php
declare(strict_types=1);


namespace App\Tests\ValueObjects;

use Domain\ValueObject\Email;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class EmailTest extends TestCase
{
    public function validInputDataProvider(): array
    {
        return[
            ["username@example.com"],
            ["ibims1email.test@example.test.com"],
        ];
    }

    /**
     * @test
     */
    public function factoryConstructionException(): void
    {
        $this->expectException(InvalidArgumentException::class);
        Email::fromString("test");
    }

    /**
     * @dataProvider validInputDataProvider
     * @test
     */
    public function factoryConstruction(string $input) : void
    {
        $email = Email::fromString($input);
        $this->assertInstanceOf(Email::class, $email);
    }


}
