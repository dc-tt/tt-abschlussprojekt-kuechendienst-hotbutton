<?php
declare(strict_types=1);


namespace App\Tests\ValueObjects;

use Domain\ValueObject\Username;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class UsernameTest extends TestCase
{
    public function validInputDataProvider(): array
    {
        return [
            ["example"],
            ["test"],
            ["client_1234"]
        ];
    }

    /**
     * @test
     */
    public function factoryConstructionException(): void
    {
        $this->expectException(InvalidArgumentException::class);
        Username::fromString("");
    }

    /**
     * @dataProvider validInputDataProvider
     * @test
     */
    public function factoryConstruction(string $input) : void
    {
        $username = Username::fromString($input);
        $this->assertInstanceOf(Username::class, $username);
    }
}
