#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266WiFiMulti.h>
#include <WiFiClient.h>

const short int LED_BLUE = 2; //GPIO 2
const short int LED_RED = 16; // GPIO 16

const char* deviceHostname = "Hotbutton-Hub";
const char* wifiHostname = deviceHostname;

const char* clientname = "client_main";
const char* clientsecret = "1234";

const char* ssid     = "FRITZ!Box 7362 SL";
const char* password = "6944434618";

const int httpPort = 80;
const int httpsPort = 443;

const char* httpIP = "34.89.216.237";
const char* httpHost = "34.89.216.237"; // without protocol login.advolution.de
const char* mailerPath = "/api/mailer";
const char* authenticatePath = "/api/token/client";

const short int BUTTON_IN = 12;
const unsigned long BUTTON_PRESS_COOLDOWN = 10000; // Cooldown in Milliseconds
const long AUTHENTICATION_COOLDOWN = 3600000;

String jwt;

unsigned long lastPress = 0;
unsigned long lastAuthentication = 0;


void setup() {
  Serial.begin(115200);
  Serial.println("Start Setup");
  definePins();
  digitalWrite(LED_BLUE, HIGH);
  digitalWrite(LED_RED, HIGH);
  connectWifi();
}


void loop() {

  if (WiFi.status() == WL_CONNECTION_LOST || WiFi.status() == WL_DISCONNECTED ) {
    connectWifi();
  }

  if (isButtonPressed() && !canMakeAnother()) {
    blink();
  }

  if (isButtonPressed() && canMakeAnotherAuthentication())
  {
    jwt = authenticate();
    lastAuthentication = millis();
  }

  if (isButtonPressed() && canMakeAnother())
  {
    sendMail();
    //blink();
    lastPress = millis();
  }
}

/**
   DEFINES PIN MODES (GPIO)
*/
void definePins() {
  pinMode(LED_BLUE, OUTPUT);
  pinMode(LED_RED, OUTPUT);
  pinMode(BUTTON_IN, INPUT_PULLUP);

}

/**
   Wenn der button gedrückt ist gehts los
*/
bool isButtonPressed() {

  if (digitalRead(BUTTON_IN) == 0) {
    return true;
  }
  return false;
}

/**
   LED Configuration
*/
void blink() {

  digitalWrite(LED_BLUE, HIGH);// turn the LED on (HIGH is the voltage level)
  delay(100);
  digitalWrite(LED_RED, HIGH);
  delay(100);              // wait for a second
  digitalWrite(LED_BLUE, LOW);
  delay(100); // turn the LED off by making the voltage LOW
  digitalWrite(LED_RED, LOW);
  delay(100);              // wait for a second
  digitalWrite(LED_BLUE, HIGH);
  digitalWrite(LED_RED, HIGH);
}


/**
   Authentication JWT
*/
String authenticate()
{
    WiFiClient client;
    HTTPClient http;

    Serial.print("[HTTP] begin...\n");
    // configure traged server and url
    http.begin(client, "http://34.89.216.237/api/token/client"); //HTTP
    http.addHeader("Content-Type", "application/json");

    Serial.print("[HTTP] POST...\n");
    // start connection and send HTTP header and body
    int httpCode = http.POST("{\"username\":\"main\", \"password\":\"1234\"}");

    // httpCode will be negative on error
    if (httpCode > 0) {
      // HTTP header has been send and Server response header has been handled
      Serial.printf("[HTTP] POST... code: %d\n", httpCode);

      // JWT response
      if (httpCode == HTTP_CODE_OK) {
        const String& payload = http.getString();
        Serial.println("received payload:\n<<");
        Serial.println(payload);
        Serial.println(">>");
        return payload;
      }
    } else {
      Serial.printf("[HTTP] POST... failed, error: %s\n", http.errorToString(httpCode).c_str());
    }

    http.end();
}


void sendMail(){
    WiFiClient client;
    HTTPClient http;

    Serial.print("[HTTP] begin...\n");
    // configure traged server and url
    http.begin(client, "http://34.89.216.237/api/mailer"); //HTTP
    http.addHeader("authorization", "Bearer "+jwt);

    Serial.print("[HTTP] POST...\n");
    // start connection and send HTTP header and body
    int httpCode = http.POST("");

    // httpCode will be negative on error
    if (httpCode > 0) {
      // HTTP header has been send and Server response header has been handled
      Serial.printf("[HTTP] POST... code: %d\n", httpCode);

      //Email sended response
      if (httpCode == HTTP_CODE_OK) {
        const String& payload = http.getString();
        Serial.println("received payload:\n<<");
        Serial.println(payload);
        Serial.println(">>");
      }
    } else {
      Serial.printf("[HTTP] POST... failed, error: %s\n", http.errorToString(httpCode).c_str());
    }

    http.end();
}


/**
   Verbindung mit dem WLAN.

   Wenn die verbindung da ist bleibt der LED auf.
   wenn nicht blinkt der LED.
*/
void connectWifi() {
  Serial.print("Connecting to ");
  Serial.println(ssid);

  /* Explicitly set the ESP8266 to be a WiFi-client, otherwise, it by default,
     would try to act as both a client and an access-point and could cause
     network-issues with your other WiFi-devices on your WiFi-network. */
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    digitalWrite(LED_RED, LOW);
    delay(250);
    Serial.print(".");
    digitalWrite(LED_RED, HIGH);
    delay(250);
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

bool canMakeAnother() {

  if ( (lastPress + BUTTON_PRESS_COOLDOWN) <= millis() || lastPress == 0)
  {
    return true;
  }
  return false;
}

bool canMakeAnotherAuthentication() {

  if ( (lastAuthentication + AUTHENTICATION_COOLDOWN) <= millis() || lastAuthentication == 0)
  {
    return true;
  }
  return false;
}
