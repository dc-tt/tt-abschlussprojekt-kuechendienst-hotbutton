# Küchendienst - Hotbutton

Die Firma DIGITAL CONTROL GmbH befindet sich in einer Bürogemeinschaft mit insgesamt ca. 100 Mitarbeitern, verteilt über zwei Stockwerke.  
In den Büroräumen gibt es zwei Küchen, für die jeweils ein Team (Büroraum oder organisatorische Gruppe) pro Kalenderwoche Küchendienst hat – sich also um Reinhaltung, Geschirrspülmaschine, Kaffeemaschine und Lagerbestand (Kaffee, Milch, etc.) kümmert.  
In der Praxis hakt es an einer regelmäßigen Erfüllung des Küchendienstes, weil die zuständigen Mitarbeiter mit ihrem Tagesgeschäft beschäftigt sind, oder versäumen, zum Wochenanfang auf den Listenaushang mit dem Küchendienstplan zu schauen. Dies führt u.a. nach der Mittagszeit dazu, dass sich gebrauchtes Geschirr in den Küchen stapelt.  
Der Küchendienstplan soll digitalisiert werden, zudem soll eine Möglichkeit geschaffen werden, das zuständige Team per „Hotbutton“ darüber zu informieren, dass eine Aktion notwendig ist und der plan bereit in ein webbasierte server zu stellen.  
Durch die Realisierung dieses Projektes soll eine Schnittstelle nach dem Representational State Transfer Paradigma zur Verfügung gestellt werden, um eine einfache Maschine-zu-Maschine-Kommunikation zu ermöglichen.  
  
Ziel des Projektes ist die Erstellung einer Web-Anwendung mit grafischer Benutzeroberfläche und Anbindung an eine Datenbank, die die im nächsten Punkt genannten Anforderungen umsetzt und ein Mikrocontroller für den Client, welcher in dem Gehäuse des Grobhandtasters verbaut wird.   
  
Folgende Anforderungen sollen durch das Projekt erfüllt werden:  
  
### Serverseitig
* Die Hotbutton-Clients, Mitarbeiter, Gruppenzuordnungen und Küchendienstpläne werden in einer Datenbank gespeichert.
* Die Anwendung erhält ein browserbasiertes Benutzerinterface.
* Mitarbeiter können als Verwalter definiert werden und haben somit Zugriff auf das Benutzerinterface.
* Verwalter können sich über Benutzername und Passwort im Benutzerinterface anmelden.
* Im Benutzerinterface können die Hotbutton-Clients, Mitarbeiter, Gruppenzuordnungen und Küchendienstpläne angelegt, eingesehen, bearbeitet und gelöscht werden.
* Zu Beginn jedes neuen Küchendienstintervalls erhält die zuständige Mitarbeitergruppe per cron-job automatistert eine Benachrichtigungsmail.
* Der Server bietet ein RESTFUL API für die kommunikation mit den Client.
* Die Authentifizierung der Clients erfolgt durch Client-ID und Secret sowie durch JSON Web Tokens (JWT).
* Der Server nimmt die Button-Events der Clients entgegen, schreibt die Aufrufe in ein Log, validiert die Events (Mehrfachaufrufe innerhalb eines definierten Zeitraums u.ä.) und verschickt bei erfolgreicher Validierung eine Benachrichtigungs-E-Mail an die zuständige Mitarbeitergruppe.
* Der E-Mail-Versand wird ebenfalls in ein Log geschrieben.
* Die Logs können im Benutzerinterface eingesehen werden.
* Realisierung mit PHP/Symfony, HTML, Javascript, LESS/CSS.

### Clientseitig
* Der Button wird über die IO-Pins mit dem Mikrocontroller verbunden.
* Der Client verbindet sich mit einem der öffentlichen WiFi-Hotspots im Gebäude.
* Der Client verbindet sich mit dem Server, über HTTP/REST authentifiziert sich per Client-ID und Secret, nimmt das JSON Web Token (JWT) entgegen und verwendet dieses für die weitere Kommunikation mit dem Server.
* Kontinuierliche Abfrage des Button-Status.
* Event “Button gedrückt” validieren (Mehrfachaufrufe innerhalb eines definierten Zeitraums, Prellen des Tasterkontakts u.ä.) und bei erfolgreicher Validierung an den Server weitergeben.
* Zeitpunkt der letzten Meldung an den Server für die nächste Validierung speichern.
* Realisierung mit C/C++ (Arduino Programming Language).
