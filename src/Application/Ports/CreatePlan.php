<?php
declare(strict_types=1);


namespace Application\Ports;

use Doctrine\ORM\EntityManagerInterface;
use Domain\Aggregate\Plan;
use Domain\Entity\Client;
use Domain\Entity\Team;
use Domain\ValueObject\Kw;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class CreatePlan
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * CreatePlan constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Plan Creation
     * @param Request $request
     * @return RedirectResponse
     */
    public function create(Request $request): RedirectResponse
    {
        //Get all teams from the database
        $teams = $this->entityManager->getRepository(Team::class)->findAll();
        // Get Client from the database (we only have one right now)
        $client = $this->entityManager->getRepository(Client::class)
            ->findOneBy(array("username.username" => "client_main"));
        // Get plan from the database sorting by week and ascending
        $plans = $this->entityManager->getRepository(Plan::class)->findBy(array(), array('kw.Kw' => "ASC"));

        // clear the plan table in the database
        foreach ($plans as $plan) {
            $this->entityManager->remove($plan);
            $this->entityManager->flush();
        }

        //Create for each team an entry to the database
        $initCounter = 1;
        foreach ($teams as $team) {
            $plan = new Plan(
                $client,
                Kw::fromInteger($initCounter),
                $team
            );

            $this->entityManager->persist($plan);
            $this->entityManager->flush();

            $initCounter++;
        }

        // Iterate the existing teams in the database again and again until the whole year is complete
        $counter = count($plans) + 1;
        while ($counter <= 53) {
            foreach ($plans as $plan) {
                $plan = new Plan(
                    $plan->getClient(),
                    Kw::fromInteger($counter),
                    $plan->getTeam()
                );

                $this->entityManager->persist($plan);
                $this->entityManager->flush();

                $counter++;
            }
        }

        // redirection
        return new RedirectResponse('/');
    }
}
