<?php
declare(strict_types=1);


namespace Application\Ports;

use Doctrine\ORM\EntityManagerInterface;
use Domain\Aggregate\Plan;
use Domain\Entity\Client;
use Domain\Entity\Team;
use Domain\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTEncodeFailureException;
use Swift_Mailer;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class KitchenServicePort
 *
 * @package Application\Ports
 */
class KitchenServicePort extends AbstractController
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /** @var Swift_Mailer */
    private $swiftMailer;

    public function __construct(EntityManagerInterface $entityManager, Swift_Mailer $swiftMailer)
    {
        $this->entityManager = $entityManager;
        $this->swiftMailer = $swiftMailer;

    }

    /**
     * @return JsonResponse
     */
    public function ButtonPressed(): Response
    {
        // find the plan of this week
        $plan = $this->entityManager->getRepository(Plan::class)
            ->findOneBy(array("kw.Kw" => date('W')));
        // find the team that is responsible for this plan
        $team = $plan->getTeam();

        // update button logs
        $file = realpath("") . "/../src/Application/logs/button.log";
        $date = PHP_EOL . date("d-m-Y h:i:s");
        file_put_contents($file, $date, FILE_APPEND);

        // notify the team user that the button is pressed via email
        $this->notifyTeamUsers($team, $this->swiftMailer);

        // JSON Response
        return $this->buildResponse(Response::HTTP_OK, "Done");
    }

    /**
     * Builds a JsonResponse with the given status, content and headers.
     *
     * @param int $status
     * @param string $content
     * @param array<string,string> $headers
     *
     * @return JsonResponse
     */
    private function buildResponse(int $status, string $content, array $headers = []): JsonResponse
    {
        // Reponse without a content doesn't need any Content-Type.
        if (empty($content) === false) {
            if (in_array('Content-Type', $headers) === false || $headers['Content-Type'] !== 'application/json') {
                $headers['Content-Type'] = 'application/json';
            }
        }

        $response = JsonResponse::fromJsonString($content, $status, $headers);
        $response->setProtocolVersion('1.1');

        return $response;
    }


    /**
     * Client authentication with the given login data
     * if they are correct deliver JWT(Json Web Token) via Json Response
     *
     * @return JsonResponse
     *
     * @throws JWTEncodeFailureException
     */
    public function authenticate(Request $request, JWTEncoderInterface $JWTEncoder): JsonResponse
    {
        // convert json data to string
        $requestData = json_decode($request->getContent(), true);
        // client username
        $user = $requestData["username"];
        // client password
        $pass = $requestData["password"];
        // check if the given data are empty
        if (empty($user) || empty($pass)) {
            return $this->buildResponse(
                Response::HTTP_BAD_REQUEST,
                "Client Authentication Data needed"
            );
        }
        // find client in the database using the name
        $client = $this->entityManager->getRepository(Client::class)->findOneBy([
            "username.username" => $user
        ]);
        // check if the client is instance of client and if the password given is correct
        if (!$client instanceof Client || !$client->getPassword()->verify($pass)) {
            return $this->buildResponse(
                Response::HTTP_BAD_REQUEST,
                "Client Authentication Data not Valid!"
            );
        }

        //Token generation using the JWT Encoder
        $token = $JWTEncoder->encode([
            "id" => (string)$client->getId(),
            "username" => (string)$client->getUsername(),
            "type" => 'client',
        ]);

        // JSON Response with token
        return $this->buildResponse(Response::HTTP_OK, $token);
    }

    /**
     * E-Mail transporter
     *
     * @param User[] $users
     * @param Swift_Mailer $mailer
     */
    private function notifyTeamUsers(Team $team, Swift_Mailer $mailer): void
    {
        // convert team object to array
        $x = $team->toArray();
        // get the emails from each user in this team
        $aUsers = [];
        foreach ($team->getUsers() as $user) {
            array_push($aUsers, $user->getUsername());
        }

        // send the email with Swift Mailer to the users using the TWIG Template
        $message = (new Swift_Message('Kuechendienst Button aufgeweckt'))
            ->setFrom('tt@digital-control.biz')
            ->setTo($aUsers)
            ->setBody(
                $this->renderView(
                    'email/notification.html.twig',
                    ['team' => $team->getName()]
                ),
                'text/html'
            );

        $mailer->send($message);
    }

}
