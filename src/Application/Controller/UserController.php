<?php

namespace Application\Controller;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Domain\Entity\Team;
use Domain\Entity\User;
use Domain\ValueObject\Email;
use Domain\ValueObject\Firstname;
use Domain\ValueObject\Lastname;
use Domain\ValueObject\Password;
use Exception;
use InvalidArgumentException;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\InvalidUuidStringException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class UserController
 *
 * @property EntityManagerInterface $entityManager
 *
 * @package Application\Controller
 */
class UserController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * UserController constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Get users from database and the TWIG template for the user list and display them
     * @return Response
     */
    public function all(): Response
    {

        $allUser = $this->entityManager->getRepository(User::class)->findAll();


        return $this->render('user/index.html.twig', [
            'users' => $allUser
        ]);
    }

    /**
     * Display the TWIG template for user addition
     * @return Response
     */
    public function add(): Response
    {
        $teams = $this->entityManager->getRepository(Team::class)->findAll();

        return $this->render('user/add.html.twig', [
            'teams' => $teams
        ]);
    }

    /**
     * Display the TWIG template for user edit
     *
     * @param string $user
     *
     * @return Response
     */
    public function edit(string $user): Response
    {
        $user = $this->getUserByIdString($user);
        $teams = $this->entityManager->getRepository(Team::class)->findAll();

        return $this->render('user/edit.html.twig', [
            'user' => $user->toArray(),
            'teams' => $teams
        ]);
    }

    /**
     * Display the TWIG template for user delete
     *
     * @param string $user
     *
     * @return Response
     */
    public function delete(string $user): Response
    {
        $user = $this->getUserByIdString($user);

        return $this->render('user/delete.html.twig', [
            'user' => $user->toArray(),
        ]);
    }

    /**
     * Add new User handler.
     *
     * @param Request $oRequest
     *
     * @return RedirectResponse
     * @throws Exception
     */
    public function handleAdd(Request $oRequest): RedirectResponse
    {
        // Get path to logs for add user
        $addUserLog = realpath("") . "/../src/Application/logs/addUser.log";
        // get logged user information
        $loggedUser = $this->get('security.token_storage')->getToken()->getUser();
        // get the request body
        $request = $oRequest->request->all();
        // find team in the database
        $team = $this->getTeamByIdString($request['group']);

        try {
            // create the user object with the information provided in the request body
            $user = new User(
                Email::fromString($request['email']),
                Password::fromUnhashedString($request['password']),
                Firstname::fromString($request['firstname']),
                Lastname::fromString($request['lastname']),
                $team
            );

            // Persist the object in the database
            $this->entityManager->persist($user);
            $this->entityManager->flush();

            // update log file
            $textContent = "Benutzer (" . $loggedUser->getUsername() .
                ") : hat ein neu Benutzer mit username " . $request['email'] . " erstellt";
            $finalContent = PHP_EOL . date("d-m-Y h:i:sa") . " -> " . $textContent;
            file_put_contents($addUserLog, $finalContent, FILE_APPEND);

        } catch (InvalidArgumentException $exception) {
            // pop error message in a popup box
            $this->get('session')->getFlashBag()->add('error', $exception->getMessage());
            // update the log file
            file_put_contents(
                $addUserLog,
                PHP_EOL . date("d-m-Y h:i:sa")
                . " -> Benutzer (" . $loggedUser->getUsername() . ") : InvalidArgumentException = " .
                $exception->getMessage(),
                FILE_APPEND
            );
        } catch (UniqueConstraintViolationException $exception) {
            // pop error message in a popup box
            $this->get('session')->getFlashBag()->add(
                'error',
                'Duplicated Username or Password in the database'
            );
            // update the log file
            file_put_contents(
                $addUserLog,
                PHP_EOL . date("d-m-Y h:i:sa")
                . " -> Benutzer (" . $loggedUser->getUsername() . ") : UniqueConstraintViolationException = " .
                $exception->getPrevious()->getMessage(),
                FILE_APPEND
            );
        }

        return $this->redirectToRoute("app_user_add");
    }

    /**
     * Edit existing User handler.
     *
     * @param Request $oRequest
     * @param string $user
     *
     * @return RedirectResponse
     */
    public function handleEdit(Request $oRequest, string $user): RedirectResponse
    {
        // Get path to logs for add user
        $editUserLog = realpath("") . "/../src/Application/logs/editUser.log";
        // get logged user information
        $loggedUser = $this->get('security.token_storage')->getToken()->getUser();
        // get user id from the url link
        $user = $this->getUserByIdString($user);
        // // find the team from this user in the database
        $team = $this->entityManager->getRepository(Team::class)->find($oRequest->request->all()['team']);
        // get the request body
        $request = $oRequest->request->all();

        try {
            // update user info using the request body and the team provided from the database
            $user->patchByArray($request, $team);
        } catch (InvalidArgumentException $exception) {
            //update logs
            file_put_contents(
                $editUserLog,
                PHP_EOL . date("d-m-Y h:i:sa")
                . " -> Benutzer (" . $loggedUser->getUsername() . ") : InvalidArgumentException = " .
                $exception->getMessage(),
                FILE_APPEND
            );
            throw new BadRequestHttpException($exception->getMessage());
        }

        $this->entityManager->flush();

        //update logs
        $textContent = "Benutzer (" . $loggedUser->getUsername() .
            ") : hat der Benutzer mit username " . $request['email'] . " bearbeitet";
        $finalContent = PHP_EOL . date("d-m-Y h:i:sa") . " -> " . $textContent;
        file_put_contents($editUserLog, $finalContent, FILE_APPEND);

        // redirect
        return $this->redirectToRoute("app_user");
    }

    /**
     * @param Request $oRequest
     *
     * @return RedirectResponse
     */
    public function handleDelete(Request $oRequest): RedirectResponse
    {
        // get log path
        $deleteUserLog = realpath("") . "/../src/Application/logs/deleteUser.log";
        // get logged user information
        $loggedUser = $this->get('security.token_storage')->getToken()->getUser();
        // get the request body
        $user = $oRequest->request->all();
        // find user in the database
        $userFinder = $this->entityManager->getRepository(User::class)->find($user['id']);

        //update log
        $textContent = "Benutzer (" . $loggedUser->getUsername() .
            ") : hat ein Benutzer gelöscht -> Username: " . $userFinder->getUsername() .
            ", Team: " . $userFinder->getTeam()->getName();
        $finalContent = PHP_EOL . date("d-m-Y h:i:sa") . " -> " . $textContent;
        file_put_contents($deleteUserLog, $finalContent, FILE_APPEND);

        // persist user deletion in the database
        $this->entityManager->remove($userFinder);
        $this->entityManager->flush();

        // redirect
        return $this->redirectToRoute("app_user");
    }

    /**
     * find user in the database using the user uuid provided
     *
     * @param string $user
     *
     * @return User
     */
    private function getUserByIdString(string $user): User
    {
        try {
            // convert string to uuid object
            $id = Uuid::fromString($user);
        } catch (InvalidUuidStringException $exception) {
            throw new NotFoundHttpException("User doesn't exist");
        }
        // find user in the database using uuid
        $user = $this->entityManager->getRepository(User::class)->find($id);

        if (!$user instanceof User) {
            throw new NotFoundHttpException("User doesn't exist");
        }

        return $user;
    }

    /**
     * find team in the database using the team uuid provided
     * @param string $teamId
     *
     * @return Team
     */
    private function getTeamByIdString(string $teamId): Team
    {
        try {
            // convert string to uuid object
            $id = Uuid::fromString($teamId);
        } catch (InvalidUuidStringException $exception) {
            throw new NotFoundHttpException("User doesn't exist");
        }
        // find team in the database using uuid
        $team = $this->entityManager->getRepository(Team::class)->find($id);

        if (!$team instanceof Team) {
            throw new NotFoundHttpException("Team doesn't exist");
        }

        return $team;
    }
}
