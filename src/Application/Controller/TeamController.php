<?php
declare(strict_types=1);

namespace Application\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Domain\Aggregate\Plan;
use Domain\Entity\Team;
use Domain\Entity\User;
use Domain\ValueObject\Name;
use InvalidArgumentException;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Ramsey\Uuid\Exception\InvalidUuidStringException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Twig\TwigFunction;

/**
 * Class TeamController
 *
 * @property EntityManagerInterface $entityManager
 *
 * @package Application\Controller
 */
final class TeamController extends AbstractController
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * TeamController constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    /**
     * @return Response
     */
    public function index(): Response
    {
        $teams = $this->entityManager->getRepository(Team::class)->findAll();


        return $this->render('team/index.html.twig', [
            'teams' => $teams
        ]);
    }


    /**
     * @return Response
     */
    public function add(): Response
    {
        return $this->render('team/add.html.twig');
    }

    /**
     * @param string $team
     *
     * @return Response
     */
    public function edit(string $team): Response
    {
        $team = $this->getTeamByIdString($team);

        return $this->render('team/edit.html.twig', [
            'team' => $team->toArray(),
        ]);
    }

    /**
     * @param string $team
     *
     * @return Response
     */
    public function delete(string $team): Response
    {
        $team = $this->getTeamByIdString($team);

        return $this->render('team/delete.html.twig', [
            'team' => $team->toArray(),
        ]);
    }

    /**
     * @param Request $oRequest
     *
     * @return RedirectResponse
     */
    public function handleAdd(Request $oRequest): RedirectResponse
    {
        $addTeamLog = realpath("") . "/../src/Application/logs/addTeam.log";
        $loggedUser = $this->get('security.token_storage')->getToken()->getUser();
        $request = $oRequest->request->all();

        try {
            $team = new Team(
                Name::fromString($request['name'])
            );
            $this->entityManager->persist($team);
            $this->entityManager->flush();

            $textContent = "Benutzer (" . $loggedUser->getUsername() .
                ") : hat ein neu Team erstellt! -> id: " . $team->getId()->toString() .
                ", Name: " . $team->getName();
            $finalContent = PHP_EOL . date("d-m-Y h:i:sa") . " -> " . $textContent;
            file_put_contents($addTeamLog, $finalContent, FILE_APPEND);

        } catch (InvalidArgumentException $exception) {
            $this->get('session')->getFlashBag()->add('error', $exception->getMessage());
            file_put_contents(
                $addTeamLog,
                PHP_EOL . date("d-m-Y h:i:sa")
                . " -> Benutzer (" . $loggedUser->getUsername() . ") : InvalidArgumentException = " .
                $exception->getMessage(),
                FILE_APPEND
            );
        }

        return $this->redirectToRoute("app_plan_create");
    }

    /**
     * @param Request $oRequest
     * @param string $team
     *
     * @return RedirectResponse
     */
    public function handleEdit(Request $oRequest, string $team): RedirectResponse
    {
        $editTeamLog = realpath("") . "/../src/Application/logs/editTeam.log";
        $loggedUser = $this->get('security.token_storage')->getToken()->getUser();
        $oTeam = $this->getTeamByIdString($team);
        try {
            $oTeam->patchByArray($oRequest->request->all());
        } catch (InvalidArgumentException $exception) {
            file_put_contents(
                $editTeamLog,
                PHP_EOL . date("d-m-Y h:i:sa")
                . " -> Benutzer (" . $loggedUser->getUsername() . ") : InvalidArgumentException = " .
                $exception->getMessage(),
                FILE_APPEND
            );
            throw new BadRequestHttpException($exception->getMessage());
        }
        $this->entityManager->flush();

        $textContent = "Benutzer (" . $loggedUser->getUsername() .
            ") : hat ein Team bearbeitet -> id: " . $oTeam->getId()->toString();
        $finalContent = PHP_EOL . date("d-m-Y h:i:sa") . " -> " . $textContent;
        file_put_contents($editTeamLog, $finalContent, FILE_APPEND);

        return $this->redirectToRoute("app_team");
    }

    /**
     * @param Request $oRequest
     *
     * @return RedirectResponse
     */
    public function handleDelete(Request $oRequest): RedirectResponse
    {
        $deleteTeamLog = realpath("") . "/../src/Application/logs/deleteTeam.log";
        $loggedUser = $this->get('security.token_storage')->getToken()->getUser();
        $team = $oRequest->request->all();
        $teamFinder = $this->entityManager->getRepository(Team::class)->find($team['id']);
        $plansTeamFinder = $this->entityManager->getRepository(Plan::class)->findBy(
            array("team" => $team['id'])
        );

        foreach ($plansTeamFinder as $planTeam){
            $this->entityManager->remove($planTeam);
        }


        $textContent = "Benutzer (" . $loggedUser->getUsername() .
            ") : hat ein Team geloescht -> id: " . $teamFinder->getId()->toString() .
            ", Name: " . $teamFinder->getName();
        $finalContent = PHP_EOL . date("d-m-Y h:i:sa") . " -> " . $textContent;
        file_put_contents($deleteTeamLog, $finalContent, FILE_APPEND);

        $this->entityManager->remove($teamFinder);
        $this->entityManager->flush();


        return $this->redirectToRoute("app_plan_create");
    }

    /**
     * @param string $input
     *
     * @return Team
     */
    private function getTeamByIdString(string $input): Team
    {
        try {
            $id = Uuid::fromString($input);
        } catch (InvalidUuidStringException $exception) {
            throw new NotFoundHttpException("Team doesn't exist");
        }
        $team = $this->entityManager->getRepository(Team::class)->find($id);

        if (!$team instanceof Team) {
            throw new NotFoundHttpException("Team doesn't exist");
        }

        return $team;
    }

    /**
     * @return array
     */
    private function countTeamMembers(): array
    {
        $countMembers = array();
        $findTeamMembers = $this->entityManager->getRepository(User::class)->findAll();
        foreach ($findTeamMembers as $team) {
            var_dump($team);
            $countMembers = [
                'name' => $team->getName(),
                'count' => count($team->getTeam()['name'])
            ];
        }
        return $countMembers;
    }
}
