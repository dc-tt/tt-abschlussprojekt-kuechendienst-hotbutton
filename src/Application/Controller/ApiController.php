<?php

namespace Application\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class ApiController extends AbstractController
{
    public function index()
    {
        return JsonResponse::create("This is the API");
    }
}
