<?php
declare(strict_types=1);

namespace Application\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Domain\Aggregate\Plan;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class DashboardController
 *
 * @property EntityManagerInterface $entityManager
 *
 * @package Application\Controller
 */
final class DashboardController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * DashboardController constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return Response
     */
    public function index(): Response
    {
        $buttonLogs = array();
        $file = file("../src/Application/logs/button.log");
        $reverseFile = array_reverse($file);
        foreach ($reverseFile as $item) {
            $buttonLogs[] = $item;
        }

        $plans = $this->entityManager->getRepository(Plan::class)->findByKw(date('W'));
        return $this->render('dashboard/dashboard.html.twig', [
            'button_logs' => $buttonLogs,
            'plans' => $plans
        ]);
    }
}
