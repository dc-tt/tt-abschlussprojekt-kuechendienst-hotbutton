<?php
declare(strict_types=1);


namespace Application\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Domain\Aggregate\Plan;
use Domain\Entity\Client;
use Domain\Entity\Team;
use Domain\Entity\User;
use Domain\ValueObject\Kw;
use Swift_Mailer;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;

class PlanController extends AbstractController
{
    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function create()
    {
        $teams = $this->entityManager->getRepository(Team::class)->findBy(array(), array('createdAt' => "ASC"));
        $client = $this->entityManager->getRepository(Client::class)
            ->findOneBy(array("username.username" => "client_main"));
        $plans = $this->entityManager->getRepository(Plan::class)->findAll();

        foreach ($plans as $plan) {
            $this->entityManager->remove($plan);
            $this->entityManager->flush();
        }

        $initCounter = 1;
        foreach ($teams as $team) {
            $plan = new Plan(
                $client,
                Kw::fromInteger($initCounter),
                $team
            );

            $this->entityManager->persist($plan);
            $this->entityManager->flush();

            $initCounter++;
        }

        $plans = $this->entityManager->getRepository(Plan::class)->findBy(array(), array('kw.Kw' => "ASC"));
        $counter = count($plans) + 1;
        while ($counter <= 53) {
            foreach ($plans as $plan) {
                $plan = new Plan(
                    $plan->getClient(),
                    Kw::fromInteger($counter),
                    $plan->getTeam()
                );

                $this->entityManager->persist($plan);
                $this->entityManager->flush();

                $counter++;
            }
        }

        return $this->redirectToRoute('app_dashboard');
    }


    /**
     * E-Mail transporter
     *
     * @param User[] $users
     * @param Swift_Mailer $mailer
     */
    private function notifyTeamUsersWithCronJob(Swift_Mailer $mailer): void
    {
        // find the plan of this week
        $plan = $this->entityManager->getRepository(Plan::class)
            ->findOneBy(array("kw.Kw" => date('W')));
        // find the team that is responsible for this plan
        $team = $plan->getTeam();

        // get the emails from each user in this team
        $aUsers = [];
        foreach ($team->getUsers() as $user) {
            array_push($aUsers, $user->getUsername());
        }

        // send the email with Swift Mailer to the users using the TWIG Template
        $message = (new Swift_Message('Kuechendienst!!'))
            ->setFrom('tt@digital-control.biz')
            ->setTo($aUsers)
            ->setBody(
                $this->renderView(
                    'email/cronJobEmail.html.twig',
                    ['team' => $team->getName()]
                ),
                'text/html'
            );

        $mailer->send($message);
    }
}
