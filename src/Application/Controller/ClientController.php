<?php

namespace Application\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Domain\Aggregate\Plan;
use Domain\Entity\Client;
use Domain\ValueObject\ClientLabel;
use Domain\ValueObject\ClientName;
use Domain\ValueObject\ClientSecret;
use Domain\ValueObject\Description;
use Domain\ValueObject\Name;
use Domain\ValueObject\Password;
use Domain\ValueObject\Username;
use InvalidArgumentException;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\InvalidUuidStringException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ClientController
 *
 * @property EntityManagerInterface $entityManager
 *
 * @package Application\Controller
 */
class ClientController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * ClientController constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return Response
     */
    public function all(): Response
    {
        $clients = $this->entityManager->getRepository(Client::class)->findAll();


        return $this->render('client/index.html.twig', [
            'clients' => $clients
        ]);
    }

    /**
     * @return Response
     */
    public function add(): Response
    {
        $clients = $this->entityManager->getRepository(Client::class)->findAll();

        return $this->render('client/add.html.twig', [
            'clients' => $clients
        ]);
    }

    /**
     * @param string $client
     *
     * @return Response
     */
    public function edit(string $client): Response
    {
        $client = $this->getClientByIdString($client);

        return $this->render('client/edit.html.twig', [
            'client' => $client->toArray()
        ]);
    }

    /**
     * @param string $client
     *
     * @return Response
     */
    public function delete(string $client): Response
    {
        $client = $this->getClientByIdString($client);

        return $this->render('client/delete.html.twig', [
            'client' => $client->toArray(),
        ]);
    }

    /**
     * @param Request $oRequest
     *
     * @return RedirectResponse
     */
    public function handleAdd(Request $oRequest): RedirectResponse
    {
        $addClientLog = realpath("") . "/../src/Application/logs/addClient.log";
        $loggedUser = $this->get('security.token_storage')->getToken()->getUser();
        $request = $oRequest->request->all();

        try {
            $client = new Client(
                Username::fromString($request['name']),
                Password::fromUnhashedString($request['password']),
            );
            $this->entityManager->persist($client);
            $this->entityManager->flush();

            $textContent = "Benutzer (" . $loggedUser->getUsername() .
                ") : hat ein neu Client mit name " . $request['name'] . " erstellt";
            $finalContent = PHP_EOL . date("d-m-Y h:i:sa") . " -> " . $textContent;
            file_put_contents($addClientLog, $finalContent, FILE_APPEND);

        } catch (InvalidArgumentException $exception) {
            $this->get('session')->getFlashBag()->add('error', $exception->getMessage());
            file_put_contents(
                $addClientLog,
                PHP_EOL . date("d-m-Y h:i:sa")
                . " -> Benutzer (" . $loggedUser->getUsername() . ") : InvalidArgumentException = " .
                $exception->getMessage(),
                FILE_APPEND
            );
        }

        return $this->redirectToRoute("app_client");
    }

    /**
     * @param Request $oRequest
     * @param string $client
     *
     * @return RedirectResponse
     */
    public function handleEdit(Request $oRequest, string $client): RedirectResponse
    {
        $editClientLog = realpath("") . "/../src/Application/logs/editClient.log";
        $loggedUser = $this->get('security.token_storage')->getToken()->getUser();
        $client = $this->getClientByIdString($client);

        try {
            $client->patchByArray($oRequest->request->all());
        } catch (InvalidArgumentException $exception) {
            file_put_contents(
                $editClientLog,
                PHP_EOL . date("d-m-Y h:i:sa")
                . " -> Benutzer (" . $loggedUser->getUsername() . ") : InvalidArgumentException = " .
                $exception->getMessage(),
                FILE_APPEND
            );
            throw new BadRequestHttpException($exception->getMessage());
        }
        $this->entityManager->flush();

        $textContent = "Benutzer (" . $loggedUser->getUsername() .
            ") : hat der Client mit name " . $client->getUsername() . " bearbeitet";
        $finalContent = PHP_EOL . date("d-m-Y h:i:sa") . " -> " . $textContent;
        file_put_contents($editClientLog, $finalContent, FILE_APPEND);

        return $this->redirectToRoute("app_client");
    }

    /**
     * @param Request $oRequest
     *
     * @return RedirectResponse
     */
    public function handleDelete(Request $oRequest): RedirectResponse
    {
        $deleteClientLog = realpath("") . "/../src/Application/logs/deleteClient.log";
        $loggedUser = $this->get('security.token_storage')->getToken()->getUser();
        $client = $oRequest->request->all();
        $clientFinder = $this->entityManager->getRepository(Client::class)->find($client['id']);

        $plan = $this->entityManager->getRepository(Plan::class)->findBy(array('client' => $client['id']));
        if (!empty($plan)) {
            $plan[0]->removeClient();
        }
        $this->entityManager->remove($clientFinder);
        $this->entityManager->flush();

        $textContent = "Benutzer (" . $loggedUser->getUsername() .
            ") : hat der Client mit name " . $clientFinder->getUsername() . " gelöscht";
        $finalContent = PHP_EOL . date("d-m-Y h:i:sa") . " -> " . $textContent;
        file_put_contents($deleteClientLog, $finalContent, FILE_APPEND);

        return $this->redirectToRoute("app_client");
    }

    /**
     * @param string $client
     *
     * @return Client
     */
    private function getClientByIdString(string $client): Client
    {
        try {
            $id = Uuid::fromString($client);
        } catch (InvalidUuidStringException $exception) {
            throw new NotFoundHttpException("Client doesn't exist");
        }
        $client = $this->entityManager->getRepository(Client::class)->find($id);

        if (!$client instanceof Client) {
            throw new NotFoundHttpException("Client doesn't exist");
        }

        return $client;
    }

}
