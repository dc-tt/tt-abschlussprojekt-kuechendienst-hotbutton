<?php
declare(strict_types=1);


namespace Domain\Aggregate;

use Domain\Entity\Client;
use Domain\Entity\Team;
use Domain\ValueObject\Kw;
use Ramsey\Uuid\UuidInterface;

class Plan
{
    /** @var UuidInterface */
    private $id;

    /** @var Team */
    private $team;

    /** @var Client */
    private $client;

    /** @var Kw */
    private $kw;

    /**
     * Plan constructor.
     * @param Client $client
     * @param Kw $kw
     * @param Team $team
     */
    public function __construct(Client $client, Kw $kw, Team $team)
    {
        $this->team = $team;
        $this->client = $client;
        $this->kw = $kw;
    }


    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @param UuidInterface $id
     */
    public function setId(UuidInterface $id): void
    {
        $this->id = $id;
    }

    /**
     * @return Team
     */
    public function getTeam(): Team
    {
        return $this->team;
    }

    /**
     * @return Client
     */
    public function getClient(): Client
    {
        return $this->client;
    }

    /**
     * @param Client $client
     */
    public function setClient(Client $client): void
    {
        $this->client = $client;
    }

    /**
     * @param Kw $kw
     */
    public function setKw(Kw $kw): void
    {
        $this->kw = $kw;
    }

    public function toArray(): array
    {
        return [
            "id" => $this->id->toString(),
            "client" => $this->client->getUsername(),
            "team" => $this->team->getName(),
            "kw" => $this->kw->toInteger()
        ];
    }
}
