<?php
declare(strict_types=1);

namespace Domain\Assert;

use InvalidArgumentException;

/**
 * Class Assert
 *
 * @package Domain\Assert
 */
final class Assert extends \Webmozart\Assert\Assert
{

    /**
     * Password have to be at least 6 chars long. 1 Uppercase, 1 Special char and 1 Number.
     *
     * @param string $sPassword
     * @param string|null $sMessage
     *
     * @throws InvalidArgumentException
     */
    public static function password(string $sPassword, ?string $sMessage = null)
    {
        $sPasswordRegex = '/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).{6,25}$/';

        if (!preg_match($sPasswordRegex, $sPassword)) {
            if (is_null($sMessage)) {
                $sMessage = "Password have to be at least 6 chars long. 1 Uppercase, 1 Special char and 1 Number.";
            }
            self::reportInvalidArgument($sMessage);
        }
    }

    /**
     * @param string $sPassword
     * @param string|null $sMessage
     *
     * @throws InvalidArgumentException
     */
    public static function hashedPassword(string $sPassword, ?string $sMessage = null)
    {
        if (!preg_match('/^\$2[ayb]\$.{56}$/', $sPassword)) {
            if (is_null($sMessage)) {
                $sMessage = "Not a valid bcrypt Password";
            }
            self::reportInvalidArgument($sMessage);
        }

    }

    /**
     * @param string $sUsername
     * @param string|null $sMessage
     *
     * @throws InvalidArgumentException
     */
    public static function username(string $sUsername, ?string $sMessage = null)
    {
        if (is_null($sUsername)) {
            $sMessage = "Not a valid Username";
        }
    }

    /**
     * @param string $sFirstname
     * @param string|null $sMessage
     *
     * @throws InvalidArgumentException
     */
    public static function firstname(string $sFirstname, ?string $sMessage = null)
    {
        Assert::noNumber($sFirstname, $sMessage);
    }

    /**
     * @param string $string
     * @param string|null $sMessage
     *
     * @throws InvalidArgumentException
     */
    public static function noNumber(string $string, ?string $sMessage = null)
    {
        if (preg_match('/\d$/', $string)) {
            if (is_null($sMessage)) {
                $sMessage = "String should not Contain any number!";
            }
            self::reportInvalidArgument($sMessage);
        }
    }

    /**
     * @param string $sLastname
     * @param string|null $sMessage
     *
     * @throws InvalidArgumentException
     */
    public static function lastName(string $sLastname, ?string $sMessage = null)
    {
        Assert::noNumber($sLastname, $sMessage);
    }
}
