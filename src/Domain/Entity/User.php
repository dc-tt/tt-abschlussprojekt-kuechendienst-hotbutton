<?php
declare(strict_types=1);

namespace Domain\Entity;

use Domain\ValueObject\Email;
use Domain\ValueObject\Firstname;
use Domain\ValueObject\Lastname;
use Domain\ValueObject\Password;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class User implements UserInterface
{
    /** @var UuidInterface */
    private $id;

    /** @var Email  */
    private $email;

    /** @var Password  */
    private $password;

    /** @var Firstname  */
    private $firstname;

    /** @var Lastname  */
    private $lastname;

    /** @var Team */
    private $team;

    /**
     * @var array
     */
    private $roles = [];

    /**
     * User constructor.
     * @param Email $email
     * @param Password $password
     * @param Firstname $firstname
     * @param Lastname $lastname
     */
    public function __construct(
        Email $email,
        Password $password,
        Firstname $firstname,
        Lastname $lastname,
        Team $team
    )
    {
        $this->email     = $email;
        $this->password  = $password;
        $this->firstname = $firstname;
        $this->lastname  = $lastname;
        $this->team      = $team;
        $this->roles     = "ROLE_USER";
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @return Firstname
     */
    public function getFirstname(): Firstname
    {
        return $this->firstname;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles[] = $this->roles;

        return array_unique($roles);
    }

    /**
     * @return Team
     */
    public function getTeam(): Team
    {
        return $this->team;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @param array $aUser
     * @param Team $team
     */
    public function patchByArray(array $aUser, Team $team): void
    {
        $email = Email::fromString($aUser["email"]);
        $firstname = Firstname::fromString($aUser["firstname"]);
        $lastname = Lastname::fromString($aUser["lastname"]);


        $this->email = $email;
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->team = $team;
    }

    /**
     * Returns the object as Array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            "id" => $this->id,
            "team" => $this->team,
            "email" => $this->email,
            "firstname" => $this->firstname,
            "lastname" => $this->lastname,
        ];
    }
}
