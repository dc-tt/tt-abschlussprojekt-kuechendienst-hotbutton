<?php
declare(strict_types=1);


namespace Domain\Entity;

use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Domain\Aggregate\Plan;
use Domain\ValueObject\Name;
use Exception;
use InvalidArgumentException;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Validator\Constraints\Collection;

class Team
{
    /** @var UuidInterface */
    private $id;

    /** @var Collection|User[] */
    private $users;

    /** @var Name */
    private $name;

    /** @var DateTimeImmutable */
    private $createdAt;

    /** @var Plan */
    private $plan;

    /**
     * Team constructor.
     * @param Name $name
     * @throws Exception
     */
    public function __construct(Name $name)
    {
        $this->users = new ArrayCollection();
        $this->name = $name;
        $this->createdAt = new DateTimeImmutable();
    }

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return User[]|Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * Returns the object a Array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "users" => $this->users,
            "createdAt" => $this->createdAt
        ];
    }

    /**
     * @param array $aTeam
     *
     * @throws InvalidArgumentException
     */
    public function patchByArray(array $aTeam): void
    {
        $name = Name::fromString($aTeam['name']);

        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getUserCount(): int
    {
        return count($this->users);
    }
}
