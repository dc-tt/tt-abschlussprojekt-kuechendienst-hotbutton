<?php
declare(strict_types=1);


namespace Domain\Entity;

use Domain\Aggregate\Plan;
use Domain\ValueObject\Name;
use Domain\ValueObject\Password;
use Domain\ValueObject\Username;
use InvalidArgumentException;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class Client implements UserInterface
{
    /** @var UuidInterface */
    private $id;

    /** @var Username */
    private $username;

    /** @var Password */
    private $password;

    /** @var array */
    private $roles = [];

    /** @var Plan */
    private $plan;

    /**
     * Client constructor.
     * @param Username $username
     * @param Password $password
     */
    public function __construct(Username $username, Password $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string)$this->username;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_CLIENT';

        return array_unique($roles);
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): Password
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * Returns the object a Array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            "id" => $this->id,
            "name" => $this->username
        ];
    }

    /**
     * @param array $aClient
     *
     * @throws InvalidArgumentException
     */
    public function patchByArray(array $aClient): void
    {
        $username = Username::fromString($aClient['name']);

        $this->username = $username;
    }

}
