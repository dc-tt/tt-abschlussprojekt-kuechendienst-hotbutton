<?php
declare(strict_types=1);

namespace Domain\ValueObject;

use Domain\Assert\Assert;
use InvalidArgumentException;

final class Username
{
    /** @var string  */
    private $username;

    /**
     * Username constructor.
     * @param string $sUsername
     */
    private function __construct(string $sUsername)
    {
        $this->username = $sUsername;
    }

    /**
     * @param string $sUsername
     * @return static
     *
     * @throws InvalidArgumentException
     */
    public static function fromString(string $sUsername): self
    {
        Assert::notEmpty($sUsername);
        Assert::string($sUsername);
        Assert::username($sUsername);

        return new self($sUsername);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->username;
    }
}
