<?php
declare(strict_types=1);

namespace Domain\ValueObject;

use Domain\Assert\Assert;
use InvalidArgumentException;

final class Password
{

    /** @var string  */
    private $password;

    /**
     * Password constructor.
     * @param string $password
     */
    private function __construct(string $password)
    {
        $this->password = $password;
    }

    /**
     * @param string $password
     * @return static
     *
     * @throws InvalidArgumentException
     */
    public static function fromUnhashedString(string $password): self
    {

        Assert::string($password);
        Assert::password($password);

        $hashedPassword = password_hash($password,PASSWORD_DEFAULT);

        return new self($hashedPassword);
    }

    /**
     * @param string $password
     * @return static
     *
     * @throws InvalidArgumentException
     */
    public static function fromHashedString(string $password): self{

        Assert::notEmpty($password);
        Assert::string($password);
        Assert::hashedPassword($password);

        return new self($password);
    }

    /**
     * @param string $password
     * @return bool
     */
    public function verify(string $password) : bool{

        return password_verify($password, $this->password);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->password;
    }
}
