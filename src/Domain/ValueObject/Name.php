<?php
declare(strict_types=1);

namespace Domain\ValueObject;

use Domain\Assert\Assert;
use InvalidArgumentException;

final class Name
{
    /** @var string */
    private $name;

    /**
     * Name constructor.
     * @param string $name
     */
    private function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @param string $name
     * @return static
     *
     *
     * @throws InvalidArgumentException
     */
    public static function fromString(string $name): self
    {

        Assert::string($name);
        Assert::notEmpty($name);
        return new self($name);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->name;
    }
}
