<?php
declare(strict_types=1);

namespace Domain\ValueObject;

use InvalidArgumentException;
use Domain\Assert\Assert;

final class Email
{
    /** @var string  */
    private $email;

    /**
     * Email constructor.
     * @param string $email
     */
    private function __construct(string $email)
    {
        $this->email = $email;
    }

    /**
     * @param string $email
     * @return static
     *
     * @throws InvalidArgumentException
     */
    public static function fromString(string $email): self
    {
        Assert::string($email);
        Assert::email($email);

        return new self($email);
    }

    /**
     * @return string
     */
    public function __toString():string
    {
        return $this->email;
    }
}
