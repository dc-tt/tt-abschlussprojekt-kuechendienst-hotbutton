<?php
declare(strict_types=1);

namespace Domain\ValueObject;

use Domain\Assert\Assert;
use InvalidArgumentException;

final class Lastname
{

    /** @var string  */
    private $lastname;

    /**
     * Lastname constructor.
     * @param string $lastname
     */
    private function __construct(string $lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @param string $lastname
     * @return static
     *
     * @throws InvalidArgumentException
     */
    public static function fromString(string $lastname): self
    {
        Assert::notEmpty($lastname);
        Assert::string($lastname);
        Assert::lastName($lastname,"Invalid Lastname");


        return new self($lastname);
    }

    public function __toString(): string
    {
        return $this->lastname;
    }


}
