<?php
declare(strict_types=1);

namespace Domain\ValueObject;

use Domain\Assert\Assert;
use InvalidArgumentException;

final class Kw
{
    /**
     * @var integer
     */
    private $Kw;

    private function __construct(int $Kw)
    {
        $this->Kw = $Kw;
    }

    /**
     * @param string $Kw
     *
     * @throws InvalidArgumentException
     * @return Kw
     */
    public static function fromString(string $Kw): self
    {
        Assert::notEmpty($Kw);
        Assert::string($Kw);


        return self::fromInteger(intval($Kw));
    }


    /**
     * @param int $Kw
     *
     * @throws InvalidArgumentException
     * @return Kw
     */
    public static function fromInteger(int $Kw): self
    {
        Assert::notEmpty($Kw);
        Assert::integer($Kw);
        Assert::greaterThan($Kw, 0);


        return new self($Kw);
    }

    /**
     * @return Int
     */
    public function toInteger(): int
    {
        return $this->Kw;
    }


    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string)$this->Kw;
    }
}
