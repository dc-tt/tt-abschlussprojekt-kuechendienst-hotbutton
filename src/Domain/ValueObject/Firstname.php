<?php
declare(strict_types=1);

namespace Domain\ValueObject;

use InvalidArgumentException;
use Domain\Assert\Assert;

final class Firstname
{
    /** @var string  */
    private $firstname;

    /**
     * Firstname constructor.
     * @param string $firstname
     */
    private function __construct(string $firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @param string $firstname
     * @return static
     *
     * @throws InvalidArgumentException
     */
    public static function fromString(string $firstname): self
    {
        Assert::string($firstname);
        Assert::notEmpty($firstname);
        Assert::firstname($firstname, "Firstname is Invalid");


        return new self($firstname);
    }

    public function __toString(): string
    {
        return $this->firstname;
    }


}
