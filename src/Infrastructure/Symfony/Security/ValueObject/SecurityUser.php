<?php
declare(strict_types=1);

namespace Infrastructure\Symfony\Security\ValueObject;

use Domain\Entity\User;
use Domain\ValueObject\Password;
use Domain\ValueObject\Username;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class SecurityUser
 *
 * @package Infrastructure\Symfony\Security\ValueObject
 */
final class SecurityUser implements UserInterface
{
    /**
     * @var User
     */
    private $user;


    /**
     * SecurityUser constructor.
     *
     * @param User $oUser
     */
    public function __construct(User $oUser)
    {
        $this->user = $oUser;
    }

    /**
     * @inheritdoc
     * @return RoleHierarchyInterface[]
     */
    public function getRoles()
    {
        return $this->user->getRoles();
    }

    /**
     * @inheritdoc
     * @return Password
     */
    public function getPassword(): Password
    {
        return Password::fromUnhashedString($this->user->getPassword());
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * @inheritdoc
     * @return Username The username
     */
    public function getUsername()
    {
        return Username::fromString($this->user->getUsername());
    }

    /**
     * @inheritdoc
     */
    public function eraseCredentials()
    {}

    /**
     * @return User
     */
    public function getUser():User{
        return $this->user;
    }
}
