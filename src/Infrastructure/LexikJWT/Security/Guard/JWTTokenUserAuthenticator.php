<?php
declare(strict_types=1);

namespace Infrastructure\LexikJWT\Security\Guard;

use Doctrine\ORM\EntityManagerInterface;
use Domain\Entity\Client;
use Domain\Entity\User;
use InvalidArgumentException;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\UserNotFoundException;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Guard\JWTTokenAuthenticator;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\TokenExtractor\TokenExtractorInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * Class JWTTokenUserAuthenticator
 *
 * @package Infrastructure\LexikJWT\Security\Guards
 */
final class JWTTokenUserAuthenticator extends JWTTokenAuthenticator
{
    private $entityManager;

    /**
     * JWTTokenUserAuthenticator constructor.
     *
     * @param JWTTokenManagerInterface $jwtManager
     * @param EventDispatcherInterface $dispatcher
     * @param TokenExtractorInterface  $tokenExtractor
     * @param EntityManagerInterface   $oEntityManager
     */
    public function __construct(
        JWTTokenManagerInterface $jwtManager,
        EventDispatcherInterface $dispatcher,
        TokenExtractorInterface $tokenExtractor,
        EntityManagerInterface $oEntityManager
    ) {
        parent::__construct($jwtManager, $dispatcher, $tokenExtractor);
        $this->entityManager = $oEntityManager;
    }


    /**
     * Loads the user or the client to authenticate.
     *
     * @param UserProviderInterface $userProvider An user provider
     * @param array                 $payload      The token payload
     * @param string                $identity     The key from which to retrieve the user "username"
     *
     * @return UserInterface
     */
    protected function loadUser(UserProviderInterface $userProvider, array $payload, $identity): UserInterface
    {
        if(!isset($payload["type"])){
            throw new UserNotFoundException("type",null);
        }
        switch ($payload["type"]){
            case "user":
                return $this->loadDatabaseUser($identity);
            case "client":
                return $this->loadDatabaseClient($identity);
            default:
                throw new UserNotFoundException("type",$payload["type"]);
        }
    }

    /**
     * Loads the user to authenticate.
     *
     * @param string $username
     * @throws InvalidArgumentException
     * @throws UserNotFoundException
     * @return UserInterface
     */
    protected function loadDatabaseUser(string $username) : UserInterface
    {
        // get user from the database using its email
        $oUser = $this->entityManager->getRepository(User::class)
            ->findOneBy(['email.email' => $username]);
        // check if the object is type of user
        if(!$oUser instanceof User){
            throw new UserNotFoundException("username",$username);
        }
        return $oUser;
    }

    /**
     * loads the client to authenticate
     *
     * @param string $name
     *
     * @return Client|object|null
     */
    protected function loadDatabaseClient(string $name): Client
    {
        // get client from the database using its name
        $client = $this->entityManager->getRepository(Client::class)
            ->findOneBy(["username.username"=>$name]);

        // check if the object is type of client
        if(!$client instanceof Client){
            throw new UserNotFoundException("username",$name);
        }
        return $client;
    }
}
