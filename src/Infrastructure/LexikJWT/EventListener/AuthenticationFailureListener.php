<?php
declare(strict_types=1);

namespace Infrastructure\LexikJWT\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationFailureEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTExpiredEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTNotFoundEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Response\JWTAuthenticationFailureResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class AuthenticationFailureListener
 *
 * @package Infrastructure\LexikJWT
 */
final class AuthenticationFailureListener
{
    private const EXPIRED = "token.expired";
    private const AUTH_FAIL = "token.authfailed";
    private const NOTFOUND = "token.notfound";

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * AuthenticationFailureListener constructor.
     *
     * @param RouterInterface $oRouter
     */
    public function __construct(RouterInterface $oRouter)
    {
        $this->router = $oRouter;
    }

    /**
     * @param AuthenticationFailureEvent $event
     */
    public function onAuthenticationFailureResponse(AuthenticationFailureEvent $event)
    {
        $data = [
            'status'  => '401 Unauthorized',
            'message' => 'Bad credentials, please verify that your username/password are correctly set',
            'code'=>self::AUTH_FAIL
        ];

        $response = new JWTAuthenticationFailureResponse($data);

        $response->headers->add(["Location"=>$this->router->generate("client_authenticate",[],RouterInterface::ABSOLUTE_URL)]);
        $event->setResponse($response);
    }

    /**
     * @param JWTExpiredEvent $event
     */
    public function onJWTExpired(JWTExpiredEvent $event)
    {
        /** @var JWTAuthenticationFailureResponse */
        $response = $event->getResponse();

        $aData = [
            "status" =>"401 Forbidden",
            "message"=>"Token expired, renew IT",
            "code"=>self::EXPIRED,
        ];
        $response = new JsonResponse($aData,401);
        $event->setResponse($response);
    }

    /**
     * @param JWTNotFoundEvent $event
     */
    public function onJWTNotFound(JWTNotFoundEvent $event)
    {
        $data = [
            'status'  => '401 Forbidden',
            'message' => 'Missing token',
            'code'=>self::NOTFOUND,
        ];

        $response = new JsonResponse($data, 401,["Location"=>$this->router->generate("client_authenticate",[],RouterInterface::ABSOLUTE_URL)]);
        $event->setResponse($response);
    }
}