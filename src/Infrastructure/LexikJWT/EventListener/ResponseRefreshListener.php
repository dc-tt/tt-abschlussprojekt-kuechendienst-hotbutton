<?php
declare(strict_types=1);

namespace Infrastructure\LexikJWT\EventListener;

use Domain\Entity\Client;
use Domain\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTEncodeFailureException;
use Infrastructure\Symfony\Security\ValueObject\SecurityUser;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class ResponseRefreshListener
 *
 * @package Infrastructure\LexikJWT\EventListener
 */
final class ResponseRefreshListener
{
    /**
     * @var SecurityUser|Client|null
     */
    private $user;

    /**
     * @var JWTEncoderInterface
     */
    private $encoder;

    /**
     * ResponseRefreshListener constructor.
     *
     * @param TokenStorageInterface $oTokenStorage
     * @param JWTEncoderInterface   $oEncoder
     */
    public function __construct(TokenStorageInterface $oTokenStorage, JWTEncoderInterface $oEncoder)
    {
        if(!is_null($oTokenStorage->getToken())){
            $this->user = $oTokenStorage->getToken()->getUser();
        }else{
            $this->user = null;
        }
        $this->encoder = $oEncoder;
    }

    /**
     * @param ResponseEvent $event
     *
     * @throws JWTEncodeFailureException
     */
    public function onKernelResponse(ResponseEvent $event)
    {

        if($this->user instanceof User){
            $sToken = $this->encoder->encode(["username"=>(string)$this->user->getUsername(),"type"=> "user"]);
        }elseif($this->user instanceof Client){
            $sToken = $this->encoder->encode(["username"=>(string)$this->user->getUsername(),"type"=> "client"]);
        }else{
            return;
        }
        $response = $event->getResponse();
        $response->headers->add(["Authorization"=>$sToken]);
    }
}
