
FROM php:7.3.2-apache-stretch

RUN apt-get -y update \
    && apt-get -y upgrade \
    && apt-get install -y libxml2-dev zlib1g-dev libzip-dev libc-client-dev iputils-ping

RUN docker-php-ext-install zip dom pdo pdo_mysql \
    && docker-php-ext-enable zip dom pdo pdo_mysql

COPY ./ /var/www/html

COPY .docker/php/apache/vhost.conf /etc/apache2/sites-available/000-default.conf

RUN a2enmod rewrite
RUN chmod -R 777 /var/www/html/
RUN service apache2 restart

RUN curl -sS https://getcomposer.org/installer | php >/dev/null 2>&1 \
    && php composer.phar install --optimize-autoloader
