// Search Function
$(document).ready(function(){
    $("#search").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#table tbody tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
});

//SELECT 2

$(document).ready(function() {
    $('.select2').select2({
        placeholder: "Teams wählen"
    });
});
